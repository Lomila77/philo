/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/16 10:48:34 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/22 13:54:37 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "data.h"
#include "utils.h"
#include <stdlib.h>

int	init_time(t_data *data, char **arg)
{
	if (ft_atoi(arg[2], &data->time[DIE]) == ERROR
		|| ft_atoi(arg[3], &data->time[EAT]) == ERROR
		|| ft_atoi(arg[4], &data->time[SLEEP]) == ERROR)
		return (ERROR);
	if (arg[5] != NULL)
	{
		if (ft_atoi(arg[5], &data->time[EAT_END]) == ERROR)
			return (ERROR);
		if (data->time[EAT_END] < 0)
			return (ERROR);
	}
	else
		data->time[EAT_END] = -1;
	if (data->time[DIE] < 0 || data->time[EAT] < 0 || data->time[SLEEP] < 0)
		return (ERROR);
	return (SUCCESS);
}

int	initialize_struct(t_data *data, char **arg, t_thread *m_data)
{
	int	i;

	i = 0;
	data->m_fork = malloc(sizeof(pthread_mutex_t) * data->number);
	if (init_time(data, arg) == ERROR)
		return (ERROR);
	data->is_dead = FALSE;
	pthread_mutex_init(&data->mu_dead, NULL);
	pthread_mutex_init(&data->mu_print, NULL);
	pthread_mutex_init(&data->eat_max, NULL);
	data->eat = 0;
	while (i < data->number)
	{
		m_data[i].data = data;
		m_data[i].name = i + 1;
		m_data[i].eating_time = 0;
		pthread_mutex_init(&data->m_fork[i], NULL);
		pthread_mutex_init(&m_data[i].eat, NULL);
		gettimeofday(&m_data[i].last_eat, NULL);
		i++;
	}
	return (SUCCESS);
}

int	free_struct(t_data *data, pthread_t *philo, t_thread *m_data)
{
	free(data->m_fork);
	free(philo);
	free(m_data);
	return (ERROR);
}

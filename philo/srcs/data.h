/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   data.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/16 10:05:51 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/22 13:58:48 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef DATA_H
# define DATA_H

# include <pthread.h>
# include <sys/time.h>
# include <stdio.h>
# include <unistd.h>

# define WRITE 0
# define LEFT 1

# define SUCCESS 0
# define ERROR -1
# define CONTINUE 1

# define TRUE 1
# define FALSE -1

# define NO -1
# define YES 1

# define DIE 0
# define EAT 1
# define SLEEP 2
# define EAT_END 3

typedef struct s_data
{
	pthread_mutex_t	*m_fork;
	int				number;
	int				is_dead;
	pthread_mutex_t	mu_dead;
	pthread_mutex_t	mu_print;
	pthread_mutex_t	eat_max;
	int				eat;
	int				time[4];
	struct timeval	time_of_day;
}	t_data;

typedef struct s_thread
{
	t_data			*data;
	int				name;
	int				eating_time;
	pthread_mutex_t	eat;
	struct timeval	last_eat;
}	t_thread;

int	initialize_struct(t_data *data, char **arg, t_thread *m_data);
int	free_struct(t_data *data, pthread_t *philo, t_thread *m_data);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   eat.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/16 14:12:14 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/22 16:02:42 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "routine.h"
#include "mutex.h"
#include "print.h"
#include <sys/time.h>

int	thread_even(t_thread *m_data, int number)
{
	if (take_mutex(&m_data->data->m_fork[m_data->name % number]) == ERROR)
		return (ERROR);
	if (time_die(m_data->data->time[DIE], m_data) == ERROR)
	{
		drop_mutex(&m_data->data->m_fork[m_data->name % number]);
		return (ERROR);
	}
	print_status(m_data, "has taken a fork");
	if (take_mutex(&m_data->data->m_fork[m_data->name - 1]) == ERROR)
	{
		drop_mutex(&m_data->data->m_fork[m_data->name % number]);
		return (ERROR);
	}
	if (time_die(m_data->data->time[DIE], m_data) == ERROR)
	{
		drop_mutex(&m_data->data->m_fork[m_data->name % number]);
		drop_mutex(&m_data->data->m_fork[m_data->name - 1]);
		return (ERROR);
	}
	print_status(m_data, "has taken a fork");
	return (SUCCESS);
}

int	take_fork(t_thread *m_data, int number)
{
	usleep(500);
	if (take_mutex(&m_data->data->m_fork[m_data->name - 1]) == ERROR)
		return (ERROR);
	if (time_die(m_data->data->time[DIE], m_data) == ERROR)
	{
		drop_mutex(&m_data->data->m_fork[m_data->name - 1]);
		return (ERROR);
	}
	print_status(m_data, "has taken a fork");
	if (number == 1)
		return (ERROR);
	if (take_mutex(&m_data->data->m_fork[m_data->name % number]) == ERROR)
	{
		drop_mutex(&m_data->data->m_fork[m_data->name - 1]);
		return (ERROR);
	}
	if (time_die(m_data->data->time[DIE], m_data) == ERROR)
	{
		drop_mutex(&m_data->data->m_fork[m_data->name - 1]);
		drop_mutex(&m_data->data->m_fork[m_data->name % number]);
		return (ERROR);
	}
	print_status(m_data, "has taken a fork");
	return (SUCCESS);
}

void	update_lasteat(t_thread *m_data)
{
	take_mutex(&m_data->eat);
	gettimeofday(&m_data->last_eat, NULL);
	drop_mutex(&m_data->eat);
	m_data->eating_time++;
	take_mutex(&m_data->data->eat_max);
	if (m_data->eating_time == m_data->data->time[EAT_END])
		m_data->data->eat++;
	drop_mutex(&m_data->data->eat_max);
}

int	eat(t_thread *m_data)
{
	if (m_data->name % 2 == 0)
	{
		if (thread_even(m_data, m_data->data->number) == ERROR)
			return (ERROR);
	}
	else
		if (take_fork(m_data, m_data->data->number) == ERROR)
			return (ERROR);
	print_status(m_data, "is eating");
	update_lasteat(m_data);
	if (do_act(m_data, EAT) == ERROR)
	{
		drop_mutex(&m_data->data->m_fork[m_data->name - 1]);
		drop_mutex(&m_data->data->m_fork[m_data->name % m_data->data->number]);
		return (ERROR);
	}
	drop_mutex(&m_data->data->m_fork[m_data->name % m_data->data->number]);
	drop_mutex(&m_data->data->m_fork[m_data->name - 1]);
	return (SUCCESS);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/16 09:49:20 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/22 15:19:28 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "data.h"
#include "routine.h"
#include "thread.h"
#include "utils.h"

#include <stdlib.h>
#include <pthread.h>

int	check_die_thread(t_thread *m_data, t_data *data)
{
	int	i;

	(void)m_data;
	i = 0;
	while (1)
	{
		if (time_die(m_data[i].data->time[DIE], &m_data[i]) == ERROR)
			break ;
		take_mutex(&data->mu_dead);
		if (data->is_dead == TRUE)
		{
			drop_mutex(&data->mu_dead);
			break ;
		}
		drop_mutex(&data->mu_dead);
		i++;
		if (i == data->number)
			i = 0;
	}
	return (SUCCESS);
}

int	check_malloc(t_thread *m_data, pthread_t *m_philo)
{
	if (m_philo == NULL || (m_data == NULL && m_philo == NULL))
		return (ERROR);
	else if (m_data == NULL)
	{
		free(m_data);
		return (ERROR);
	}
	return (SUCCESS);
}

int	check_arg(char **argv)
{
	if (str_is_num(argv[1]) == NO || str_is_num(argv[2]) == NO
		|| str_is_num(argv[3]) == NO || str_is_num(argv[4]) == NO)
		return (ERROR);
	if (argv[5] != NULL && str_is_num(argv[5]) == NO)
		return (ERROR);
	return (SUCCESS);
}

int	main(int argc, char **argv)
{
	t_thread	*m_data;
	t_data		data;
	pthread_t	*m_philo;

	if (argc < 5 || argc > 6)
		return (ERROR);
	if (check_arg(argv) == ERROR)
		return (ERROR);
	ft_atoi(argv[1], &data.number);
	if (data.number <= 0)
		return (ERROR);
	m_philo = malloc(sizeof(pthread_t) * data.number);
	m_data = malloc(sizeof(t_thread) * data.number);
	if (check_malloc(m_data, m_philo) == ERROR)
		return (ERROR);
	if (initialize_struct(&data, argv, m_data) == ERROR)
		return (free_struct(&data, m_philo, m_data));
	gettimeofday(&data.time_of_day, NULL);
	create_thread(m_philo, m_data);
	check_die_thread(m_data, &data);
	end_thread(m_philo, data.number);
	free_struct(&data, m_philo, m_data);
	return (SUCCESS);
}

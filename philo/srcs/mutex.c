/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mutex.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 13:17:55 by gcolomer          #+#    #+#             */
/*   Updated: 2022/01/19 13:17:55 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "mutex.h"
#include "data.h"

int	take_mutex(pthread_mutex_t *mutex)
{
	if (pthread_mutex_lock(mutex) != 0)
		return (ERROR);
	return (SUCCESS);
}

int	drop_mutex(pthread_mutex_t *mutex)
{
	if (pthread_mutex_unlock(mutex) != 0)
		return (ERROR);
	return (SUCCESS);
}

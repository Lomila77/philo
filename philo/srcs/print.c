/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 13:27:45 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/22 16:10:23 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "print.h"

void	print_status(t_thread *m_data, char *str)
{
	printf("%ld %d %s\n",
		time_passed(m_data->data->time_of_day), m_data->name, str);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   print.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/19 13:30:11 by gcolomer          #+#    #+#             */
/*   Updated: 2022/01/19 13:30:11 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINT_H
# define PRINT_H

# include <stdio.h>
# include <sys/time.h>
# include "data.h"
# include "routine.h"
# include "mutex.h"

void	print_status(t_thread *m_data, char *str);

#endif

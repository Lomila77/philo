/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routine.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/16 14:36:41 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/22 15:29:22 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "routine.h"

int	routine(t_thread *m_data)
{
	gettimeofday(&m_data->last_eat, NULL);
	while (CONTINUE)
	{
		if (time_die(m_data->data->time[DIE], m_data) == ERROR)
			break ;
		if (eat(m_data) == ERROR)
			break ;
		if (time_die(m_data->data->time[DIE], m_data) == ERROR)
			break ;
		if (sleep_(m_data) == ERROR)
			break ;
		if (time_die(m_data->data->time[DIE], m_data) == ERROR)
			break ;
		print_status(m_data, "is thinking");
		if (time_die(m_data->data->time[DIE], m_data) == ERROR)
			break ;
	}
	return (SUCCESS);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   routine.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/16 16:19:04 by gcolomer          #+#    #+#             */
/*   Updated: 2022/01/16 16:19:04 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ROUTINE_H
# define ROUTINE_H

# include "data.h"
# include "mutex.h"
# include "print.h"

# include <sys/time.h>

int				eat(t_thread *m_data);
int				sleep_(t_thread *m_data);
int				routine(t_thread *m_data);
int				time_die(int die, t_thread *m_data);
unsigned long	time_passed(struct timeval eat_time);
int				do_act(t_thread *m_data, int act);

#endif

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sleep.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/16 17:13:52 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/22 15:32:00 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "routine.h"
#include "mutex.h"
#include "print.h"

int	sleep_(t_thread *m_data)
{
	print_status(m_data, "is sleeping");
	if (do_act(m_data, SLEEP) == ERROR)
		return (ERROR);
	if (time_die(m_data->data->time[DIE], m_data) == ERROR)
		return (ERROR);
	return (SUCCESS);
}

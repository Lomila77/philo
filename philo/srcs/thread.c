/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   thread.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/17 15:37:13 by gcolomer          #+#    #+#             */
/*   Updated: 2022/01/17 15:37:13 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "thread.h"
#include "routine.h"

#include <pthread.h>

void	create_thread(pthread_t *m_philo, t_thread *m_data)
{
	int	i;

	i = 0;
	while (i < m_data->data->number)
	{
		pthread_create(&m_philo[i], NULL, (void *)&routine, (void *)&m_data[i]);
		i++;
	}
}

void	end_thread(pthread_t *m_philo, int number)
{
	int	i;

	i = 0;
	while (i < number)
	{
		pthread_join(m_philo[i], NULL);
		i++;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   thread.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/17 15:37:22 by gcolomer          #+#    #+#             */
/*   Updated: 2022/01/17 15:37:22 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef THREAD_H
# define THREAD_H

# include "data.h"

void	create_thread(pthread_t *m_philo, t_thread *m_data);
void	end_thread(pthread_t *m_philo, int number);

#endif

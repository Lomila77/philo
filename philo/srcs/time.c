/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   time.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/01/21 14:53:23 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/22 16:11:32 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "routine.h"

int	do_act(t_thread *m_data, int act)
{
	struct timeval	begin;

	gettimeofday(&begin, NULL);
	while ((int)time_passed(begin) < m_data->data->time[act])
	{
		usleep(500);
		if (time_die(m_data->data->time[DIE], m_data) == ERROR)
			return (ERROR);
	}
	return (SUCCESS);
}

unsigned long	time_passed(struct timeval eat_time)
{
	struct timeval	now;

	gettimeofday(&now, NULL);
	return ((now.tv_sec * 1000 + now.tv_usec / 1000)
		- (eat_time.tv_sec * 1000 + eat_time.tv_usec / 1000));
}

void	dead_to_true(t_thread *m_data)
{
	take_mutex(&m_data->data->mu_dead);
	m_data->data->is_dead = TRUE;
	drop_mutex(&m_data->data->mu_dead);
	print_status(m_data, "is died");
}

int	time_die(int die, t_thread *m_data)
{
	take_mutex(&m_data->data->eat_max);
	if (m_data->data->eat >= m_data->data->number
		|| m_data->data->time[EAT_END] == 0)
	{
		drop_mutex(&m_data->data->eat_max);
		return (ERROR);
	}
	drop_mutex(&m_data->data->eat_max);
	take_mutex(&m_data->data->mu_dead);
	if (m_data->data->is_dead == TRUE)
	{
		drop_mutex(&m_data->data->mu_dead);
		return (ERROR);
	}
	drop_mutex(&m_data->data->mu_dead);
	take_mutex(&m_data->eat);
	if (time_passed(m_data->last_eat) >= (unsigned long)die)
	{
		drop_mutex(&m_data->eat);
		dead_to_true(m_data);
		return (ERROR);
	}
	drop_mutex(&m_data->eat);
	return (SUCCESS);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gcolomer <gcolomer@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/02/03 18:59:37 by gcolomer          #+#    #+#             */
/*   Updated: 2022/02/22 15:09:32 by gcolomer         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "utils.h"
#include "data.h"

int	str_is_num(char *str)
{
	int	i;

	i = 0;
	if (str[i] == '-')
		i++;
	while (str[i])
	{
		if (str[i] < '0' || str[i] > '9')
			return (NO);
		i++;
	}
	return (YES);
}

void	set_to_zero(int *i, int *val, long long *res)
{
	*i = 0;
	*val = 0;
	*res = 0;
}

int	ft_isspace(char c)
{
	if (c == '\t' || c == '\n' || c == '\v'
		|| c == '\f' || c == '\r' || c == ' ')
		return (YES);
	return (NO);
}

int	ft_atoi(const char *str, int *nb)
{
	int			i;
	int			val;
	long long	res;

	set_to_zero(&i, &val, &res);
	while (ft_isspace(str[i]) == YES)
		i++;
	if (str[i] == '-')
	{
		val = 1;
		i++;
	}
	else if (str[i] == '+')
		i++;
	while (str[i] >= '0' && str[i] <= '9' && res <= 2147483647)
	{
		res = res * 10 + (str[i] - 48);
		i++;
	}
	if (val % 2 != 0)
		res *= -1;
	if (res > 2147483647 || res < -2147483648)
		return (ERROR);
	*nb = res;
	return (SUCCESS);
}
